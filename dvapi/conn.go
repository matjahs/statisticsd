package dvapi

import (
	"encoding/json"
	"fmt"

	"src.hexon.nl/whdaemon"
)

// Conn represents a dvAPI connection to the DV frontend.
type Conn struct {
	whdaemon.Client
	env Environment
}

// Dial connects to the DV frontend.
func Dial(addr string, env Environment) (*Conn, error) {
	client, err := whdaemon.DialClient(addr)
	if err != nil {
		return nil, err
	}
	conn := &Conn{
		Client: *client,
		env:    env,
	}
	return conn, nil
}

// SetEnvironment changes the environment for subsequent API calls.
func (c *Conn) SetEnvironment(env Environment) {
	c.env = env
}

func (c *Conn) callCommand(command string, arguments ...interface{}) ([]byte, error) {
	commandArray := []interface{}{
		command,
		map[string]interface{}{
			"user":        c.env.User,
			"application": c.env.Application,
			"language":    c.env.Language,
			"timeout":     int(c.env.Timeout.Seconds()),
		},
	}
	commandArray = append(commandArray, arguments...)

	request, err := json.Marshal(commandArray)
	if err != nil {
		return nil, fmt.Errorf("error encoding command: %v", err)
	}

	response, err := c.Call(request)
	if err != nil {
		return nil, err
	}

	// Return value on success: [true, <ret>, null]
	// Return value on failure: [false, 'ExceptionClass', 'Exception message']
	// Unmarshal only the first level of the JSON message
	parsedResponse := make([]json.RawMessage, 0, 3)
	err = json.Unmarshal(response, &parsedResponse)
	if err != nil {
		return nil, fmt.Errorf("error decoding fronend response %#v: %v", string(response), err)
	}
	if len(parsedResponse) != 3 {
		return nil, fmt.Errorf("DV frontend returned invalid response: " + string(response))
	}

	var success bool
	err = json.Unmarshal(parsedResponse[0], &success)
	if err != nil {
		return nil, err
	}

	if !success {
		var exceptionClass string
		err = json.Unmarshal(parsedResponse[1], &exceptionClass)
		if err != nil {
			return nil, err
		}

		var exceptionMessage string
		err = json.Unmarshal(parsedResponse[2], &exceptionMessage)
		if err != nil {
			return nil, err
		}

		return nil, fmt.Errorf("%s failed with %s: %s", command, exceptionClass, exceptionMessage)
	}

	return parsedResponse[1], nil
}
