package dvapi

import (
	"testing"
)

func TestConnImplementsAPI(t *testing.T) {
	var _ API = &Conn{}
}
