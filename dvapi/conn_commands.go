package dvapi

import (
	"encoding/json"
	"strconv"
)

// GetAdsStatusWithCriteria retrieves the adStatus of the ads matching the supplied criteria.
// Only the fields corresponding to requestedInformation will be filled.
func (c *Conn) GetAdsStatusWithCriteria(criteria map[string]interface{}, requestedInformation []string, parameters map[string]interface{}) (map[uint64]AdStatus, error) {
	data, err := c.callCommand("getAdsStatusWithCriteria", criteria, requestedInformation, parameters)
	if err != nil {
		return nil, err
	}

	adsStatus := make(map[string]AdStatus)
	fixedAdsStatus := make(map[uint64]AdStatus)

	err = json.Unmarshal(data, &adsStatus)
	if err != nil {
		// Maybe the result set is empty?
		// An empty result set is encoded as an empty array instead of an empty map
		slice := make([]json.RawMessage, 0)
		if err := json.Unmarshal(data, &slice); err == nil {
			return fixedAdsStatus, nil
		}

		return nil, err
	}

	// Replace string keys with uint64
	for adIDString, adStatus := range adsStatus {
		adID, err := strconv.ParseUint(adIDString, 10, 64)
		if err != nil {
			return nil, err
		}
		adStatus.ID = adID
		fixedAdsStatus[adID] = adStatus
	}

	return fixedAdsStatus, nil
}
