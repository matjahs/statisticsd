package dvapi

import (
	"time"
)

// API represents a way of interfacing with DV.
//
// TODO: Use more granular interfaces for single calls.
type API interface {
	// SetEnvironment determines the environment that is sent with subsequent API calls.
	SetEnvironment(env Environment)

	// GetAdsStatusWithCriteria searches for ads that match the criteria and returns the
	// information specified in requestedInformation for each ad.
	GetAdsStatusWithCriteria(criteria map[string]interface{}, requestedInformation []string, parameters map[string]interface{}) (map[uint64]AdStatus, error)
}

// Environment describes the application that is using the API and influences the behaviour
// of API calls.
type Environment struct {
	User        string
	Application string
	Language    string
	Timeout     time.Duration
}

// AdStatus represents the current status of a single ad.
type AdStatus struct {
	ID           uint64
	ContentID    uint64
	SiteID       uint16
	SiteLongCode string
	ResellerCode string
	StatusCode   string
	BVRID        uint32
	ClientID     uint16
}
