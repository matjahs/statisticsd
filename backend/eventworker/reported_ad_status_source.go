package eventworker

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// ReportedAdStatusSource is an interface for setting the current statusCode of an ad
// and checking if it has changed from the previous one.
type ReportedAdStatusSource interface {
	Set(adID uint64, statusCode string) (changed bool, err error)
	Delete(adID uint64) error
}

// DBReportedAdStatusSource is an implementation of ReportedAdStatusSource that uses the
// database to store the last reported ad status code.
type DBReportedAdStatusSource struct {
	db               *sqlx.DB
	insertUpdateStmt *sqlx.Stmt
	deleteStmt       *sqlx.Stmt
}

// NewDBReportedAdStatusSource creates a new DBReportedAdStatusSource for an existing
// database connection.
func NewDBReportedAdStatusSource(db *sqlx.DB) (*DBReportedAdStatusSource, error) {
	insertUpdateStmt, err := db.Preparex("INSERT INTO dv.ad_last_reported_status_codes (ad_id, status_code) VALUES (?, ?) ON DUPLICATE KEY UPDATE status_code = ?")
	if err != nil {
		return nil, fmt.Errorf("Unable to prepare INSERT DB query: %v", err)
	}

	deleteStmt, err := db.Preparex("DELETE FROM dv.ad_last_reported_status_codes WHERE ad_id = ?")
	if err != nil {
		return nil, fmt.Errorf("Unable to prepare DELETE DB query: %v", err)
	}

	return &DBReportedAdStatusSource{db, insertUpdateStmt, deleteStmt}, nil
}

// Set sets the last reported ad status code to statusCode and reports if it has changed
// compared to the previous value.
func (s *DBReportedAdStatusSource) Set(adID uint64, statusCode string) (changed bool, err error) {
	res, err := s.insertUpdateStmt.Exec(adID, statusCode, statusCode)
	if err != nil {
		// Query failed (even after retrying internally if the connection has been lost)
		return false, fmt.Errorf("Error updating dv.ad_last_reported_status_codes for dvAd(%d): %v", adID, err)
	}

	switch rows, _ := res.RowsAffected(); rows {
	case 0:
		// Status code has not changed
		return false, nil
	case 1, 2:
		// Status code changed
		return true, nil
	default:
		return false, fmt.Errorf("Invalid number of affected rows while updating dv.ad_last_reported_status_codes for dvAd(%d): %v", adID, rows)
	}
}

// Delete deletes the last reported ad status code from the database, forcing Set to return
// changed = true the next time the ad is processed.
func (s *DBReportedAdStatusSource) Delete(adID uint64) error {
	if _, err := s.deleteStmt.Exec(adID); err != nil {
		return fmt.Errorf("Error deleting row in dv.ad_last_reported_status_codes for dvAd(%d): %v", adID, err)
	}
	return nil
}
