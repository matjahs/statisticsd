package eventworker

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"src.hexon.nl/bummer"
	"src.hexon.nl/dv/dvapi"
	"src.hexon.nl/events/eventclient"
	"src.hexon.nl/events/eventtype"
	"src.hexon.nl/stats"
)

type void struct{}

const jobPriority = 0

// EventWorker generates dvAdStatusChanged events by:
//  - Reading a job from the job source (Beanstalk) that the adStatus for one or more ads may have changed;
//  - Requesting the current adStatus for those ads from the DV frontend;
//  - Comparing the current adStatus with the last reported adStatus (if any);
//  - Generating an event per ad whose adStatus has changed.
type EventWorker struct {
	JobSource              JobSource
	DVAPI                  dvapi.API
	ReportedAdStatusSource ReportedAdStatusSource
	EventsConnection       eventclient.Submitter
	StatsD                 stats.Tracker

	mu          sync.Mutex
	done        chan void
	finished    chan void
	returnError error
}

// Start starts the EventWorker pipeline and returns when it is running.
func (ew *EventWorker) Start() error {
	ew.mu.Lock()
	defer ew.mu.Unlock()

	if ew.IsRunning() {
		return errors.New("already running")
	}

	// Check if all required dependencies are supplied
	if ew.JobSource == nil {
		return errors.New("JobSource dependency missing")
	}
	if ew.DVAPI == nil {
		return errors.New("DVAPI dependency missing")
	}
	if ew.ReportedAdStatusSource == nil {
		return errors.New("ReportedAdStatusSource dependency missing")
	}
	if ew.EventsConnection == nil {
		return errors.New("EventsConnection dependency missing")
	}
	if ew.StatsD == nil {
		return errors.New("StatsD dependency missing")
	}

	// Create channels
	ew.done = make(chan void)
	ew.finished = make(chan void)

	// Reserve jobs from the job source
	rawJobs := make(chan *rawJob)
	go ew.reserveJobs(ew.done, rawJobs)

	// Parse jobs from the job source
	parsedJobs := make(chan *parsedJob)
	go ew.parseJobs(rawJobs, parsedJobs)

	// Request adStatus from dvAPI
	adStatuses := make(chan *jobWithAdStatuses)
	go ew.requestAdStatuses(parsedJobs, adStatuses)

	// Compare received adStatuses with last reported adStatuses in the database
	changedAdStatuses := make(chan *jobWithAdStatuses)
	go ew.handleAdStatuses(adStatuses, changedAdStatuses)

	// Trigger evsys events for changed adStatuses
	go ew.triggerEvents(changedAdStatuses, ew.finished)

	return nil
}

// Stop stops the EventWorker pipeline and returns when it is stopping.
func (ew *EventWorker) Stop() {
	ew.mu.Lock()
	defer ew.mu.Unlock()

	if ew.done == nil {
		// Not running (or busy stopping)
		return
	}

	// Tell first goroutine in the pipeline to shut down
	close(ew.done)
	ew.done = nil
}

func (ew *EventWorker) stopWithError(err error) {
	if ew.returnError == nil {
		bummer.Errorf("Stopping EventWorker because of error: %v", err)
		ew.returnError = err
	} else {
		bummer.Errorf("Received another error while stopping EventWorker: %v", err)
	}
	ew.Stop()
}

// Join blocks until the EventWorker pipeline has finished running.
func (ew *EventWorker) Join() error {
	// Wait until the last goroutine in the pipeline closes the finished channel
	<-ew.finished

	// Return the error if there is any
	return ew.returnError
}

// IsRunning returns whether the EventWorker pipeline is currently running.
func (ew *EventWorker) IsRunning() bool {
	return ew.done != nil
}

type rawJob struct {
	ID   uint64
	Body []byte
}

func (j *rawJob) ReleaseJobID() uint64 {
	id := j.ID
	j.ID = 0
	return id
}

type parsedJob struct {
	JobID     uint64
	EventType string
	AdIDs     []uint64
	Stamp     uint64
}

func (j *parsedJob) ReleaseJobID() uint64 {
	id := j.JobID
	j.JobID = 0
	return id
}

type jobWithAdStatuses struct {
	JobID      uint64
	Stamp      uint64
	AdStatuses map[uint64]dvapi.AdStatus
}

func (j *jobWithAdStatuses) ReleaseJobID() uint64 {
	id := j.JobID
	j.JobID = 0
	return id
}

func (ew *EventWorker) reserveJobs(done <-chan void, ch chan<- *rawJob) {
	// Reserve jobs
loop:
	for {
		select {
		case <-done:
			// Done channel is closed; stop reserving jobs
			break loop
		default:
			id, body, err := ew.JobSource.GetJob(1 * time.Second)
			if err != nil {
				if ew.JobSource.IsTimeoutError(err) {
					// Expected error: reserve timeout
				} else {
					// Unexpected error; will most likely not recover from this error (eg network disconnect)
					ew.stopWithError(fmt.Errorf("Error reserving job from JobSource: %v", err))
				}
				continue
			}
			ew.StatsD.Increment("jobs_reserved")

			ch <- &rawJob{id, body}
		}
	}
	bummer.Debug("Finished reserving jobs from job source")
	close(ch)
}

func (ew *EventWorker) parseJobs(rawJobs <-chan *rawJob, parsedJobs chan<- *parsedJob) {
	for rawJob := range rawJobs {
		parsedJob := &parsedJob{
			JobID: rawJob.ID,
		}

		// Parse JSON
		err := json.Unmarshal(rawJob.Body, parsedJob)
		if err != nil {
			// Unable to parse job; discard it
			bummer.Errorf("Received job with invalid JSON from job source (discarding): %v\nBody: %q", err, rawJob.Body)
			if err := ew.JobSource.DeleteJob(rawJob.ID); err != nil {
				bummer.Errorf("Error deleting job with invalid JSON from job source: %v", err)
				// Job will stay reserved until the TTL expires, when it will be automatically released
			}
			continue
		}

		parsedJobs <- parsedJob
	}
	bummer.Debug("Finished parsing jobs")
	close(parsedJobs)
}

func (ew *EventWorker) requestAdStatuses(parsedJobs <-chan *parsedJob, adStatuses chan<- *jobWithAdStatuses) {
	for job := range parsedJobs {
		criteria := map[string]interface{}{
			"ids": job.AdIDs,
		}
		requestedInformation := []string{
			"contentId",
			"siteId",
			"siteLongCode",
			"resellerCode",
			"statusCode",
			"bvrId",
			"clientId",
		}
		parameters := map[string]interface{}{
			"allowOutdated": false,
		}

		result, err := ew.DVAPI.GetAdsStatusWithCriteria(criteria, requestedInformation, parameters)
		ew.StatsD.Increment("frontend_calls")
		if err != nil {
			// Error getting adStatus
			// TODO: Differentiate between network error and error response?
			ew.stopWithError(fmt.Errorf("Error getting adStatus for ads %v from DV frontend: %v", job.AdIDs, err))
			ew.retryJobAfterDelay(job)
			continue
		}

		// TODO: Check if all requested ads are present in the response

		adStatuses <- &jobWithAdStatuses{job.JobID, job.Stamp, result}
	}
	bummer.Debug("Finished requesting adStatuses")
	close(adStatuses)
}

type jobWithID interface {
	ReleaseJobID() uint64
}

// retryJobAfterDelay can be used if processing a job has failed and needs to be retried later.
func (ew *EventWorker) retryJobAfterDelay(job jobWithID) {
	jobID := job.ReleaseJobID()

	if jobID == 0 {
		// Job has already been released
		return
	}

	if err := ew.JobSource.ReleaseJob(jobID, jobPriority, 1*time.Minute); err != nil {
		bummer.Errorf("Error releasing job from job source: %v", err)
		// Job will stay reserved until the TTL expires, when it will be automatically released
	}
}

func (ew *EventWorker) handleAdStatuses(adStatuses <-chan *jobWithAdStatuses, triggerEvents chan<- *jobWithAdStatuses) {
	for job := range adStatuses {
		jobWithChangedAdStatuses := &jobWithAdStatuses{
			JobID:      job.JobID,
			Stamp:      job.Stamp,
			AdStatuses: make(map[uint64]dvapi.AdStatus),
		}

		for _, adStatus := range job.AdStatuses {
			changed, err := ew.ReportedAdStatusSource.Set(adStatus.ID, adStatus.StatusCode)
			if err != nil {
				// Query failed (even after retrying internally if the connection has been lost)
				ew.stopWithError(fmt.Errorf("Error updating last reported ad status for dvAd(%d): %v", adStatus.ID, err))
				ew.retryJobAfterDelay(jobWithChangedAdStatuses)

				// Continue processing the jobs in the pipeline until the EventWorker has stopped
				continue
			}

			ew.StatsD.Increment("ad_queries_executed")

			if changed {
				jobWithChangedAdStatuses.AdStatuses[adStatus.ID] = adStatus
			}
		}

		// Could have zero AdStatuses, but still needs to be deleted from job source
		triggerEvents <- jobWithChangedAdStatuses
	}
	bummer.Debug("Finished handling adStatuses")
	close(triggerEvents)
}

func (ew *EventWorker) triggerEvents(changedAdStatuses <-chan *jobWithAdStatuses, finished chan<- void) {
	for job := range changedAdStatuses {
		for _, adStatus := range job.AdStatuses {
			// Trigger event
			event := &eventtype.DVAdStatusChanged{
				SID:          adStatus.ContentID,
				AdID:         adStatus.ID,
				SiteID:       adStatus.SiteID,
				SiteLongCode: adStatus.SiteLongCode,
				ResellerCode: adStatus.ResellerCode,
				Stamp:        job.Stamp,
				StatusCode:   adStatus.StatusCode,
				BVRID:        adStatus.BVRID,
				CID:          adStatus.ClientID,
			}
			err := ew.EventsConnection.Submit(event)
			if err != nil {
				bummer.Errorf("Error submitting event for dvAd(%d): %v", adStatus.ID, err)
				// The DB state has already been updated, so to force that the next time the
				// job is processed the query has affected rows, we have to delete the row
				if err := ew.ReportedAdStatusSource.Delete(adStatus.ID); err != nil {
					bummer.Errorf("Error deleting last reported ad status for dvAd(%d) (will not be automatically fixed): %v", adStatus.ID, err)
				}
				ew.retryJobAfterDelay(job)
			}
			ew.StatsD.Increment("events_triggered")
		}

		if job.JobID > 0 {
			// Delete job from job source
			err := ew.JobSource.DeleteJob(job.JobID)
			if err != nil {
				bummer.Errorf("Error deleting job from job source: %v", err)
				// Job will stay reserved until the TTL expires, when it will be automatically released
			}
		}
	}
	bummer.Debug("Finished triggering events")
	close(finished)
}
