package eventworker

import (
	"time"

	"github.com/hexon/beanstalk"
)

// JobSource is an interface for consuming (Beanstalk) jobs.
type JobSource interface {
	GetJob(timeout time.Duration) (id uint64, body []byte, err error)
	DeleteJob(id uint64) error
	ReleaseJob(id uint64, priority uint32, delay time.Duration) error

	IsTimeoutError(err error) bool
}

// BeanstalkJobSource is an implementation of JobSource that is backed by a Beanstalk client.
type BeanstalkJobSource struct {
	conn    *beanstalk.Conn
	tubeSet *beanstalk.TubeSet
}

// DialBeanstalkJobSource connects to beanstalkd and returns a BeanstalkJobSource that
// reserves jobs from the given tubes.
func DialBeanstalkJobSource(addr string, tubes ...string) (*BeanstalkJobSource, error) {
	c, err := beanstalk.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	ts := beanstalk.NewTubeSet(c, tubes...)
	return &BeanstalkJobSource{c, ts}, nil
}

// GetJob reserves a job from Beanstalk with the given timeout.
// Use IsTimeoutError to check if the error was because the timeout elapsed.
func (js *BeanstalkJobSource) GetJob(timeout time.Duration) (id uint64, body []byte, err error) {
	return js.tubeSet.Reserve(timeout)
}

// DeleteJob deletes a job that was reserved by an earlier call to GetJob.
func (js *BeanstalkJobSource) DeleteJob(id uint64) error {
	return js.conn.Delete(id)
}

// ReleaseJob puts back a job so that it can be reserved again after a delay.
func (js *BeanstalkJobSource) ReleaseJob(id uint64, priority uint32, delay time.Duration) error {
	return js.conn.Release(id, priority, delay)
}

// IsTimeoutError returns whether an error returned by GetJob indicates an elapsed timeout.
func (js *BeanstalkJobSource) IsTimeoutError(err error) bool {
	e, ok := err.(beanstalk.ConnError)
	return ok && e.Err == beanstalk.ErrTimeout
}
