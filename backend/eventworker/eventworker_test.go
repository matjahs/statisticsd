package eventworker

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"src.hexon.nl/bummer"
	"src.hexon.nl/dv/dvapi"
	"src.hexon.nl/events"
	"src.hexon.nl/stats"
)

func init() {
	bummer.SetBackend(bummer.NOOPBackend{})
	bummer.SetDefaultApplication("go test")
}

type mockBackend struct {
	t          *testing.T
	jobHandled chan void

	AvailableJobs chan []byte
	ReleasedJobs  [][]byte

	CurrentAdStatuses map[uint64]string
	SubmittedEvents   []events.Event
	openJobs          map[uint64][]byte
	lastJobID         uint64
}

func newMockBackend(t *testing.T) *mockBackend {
	return &mockBackend{
		t:          t,
		jobHandled: make(chan void, 1),

		AvailableJobs: make(chan []byte),
		ReleasedJobs:  make([][]byte, 0),

		CurrentAdStatuses: make(map[uint64]string),
		SubmittedEvents:   make([]events.Event, 0),
		openJobs:          make(map[uint64][]byte),
	}
}

func (mb *mockBackend) HandleJob(job []byte) {
	mb.AvailableJobs <- job
	<-mb.jobHandled
}

type mockTimeout struct {
}

func (me mockTimeout) Error() string {
	return "Timeout reached"
}

// JobSource methods

func (mb *mockBackend) GetJob(timeout time.Duration) (id uint64, body []byte, err error) {
	select {
	case job := <-mb.AvailableJobs:
		mb.lastJobID++
		id := mb.lastJobID
		mb.openJobs[id] = job
		return id, job, nil
	case <-time.After(timeout):
		return 0, nil, mockTimeout{}
	}
}

func (mb *mockBackend) DeleteJob(id uint64) error {
	if _, ok := mb.openJobs[id]; !ok {
		err := fmt.Errorf("Tried to delete non-open job: %v", id)
		mb.t.Error(err)
		return err
	}
	delete(mb.openJobs, id)

	select {
	case mb.jobHandled <- void{}:
	default:
	}

	return nil
}

func (mb *mockBackend) ReleaseJob(id uint64, priority uint32, delay time.Duration) error {
	job, ok := mb.openJobs[id]
	if !ok {
		err := fmt.Errorf("Tried to release non-open job: %v", id)
		mb.t.Error(err)
		return err
	}
	delete(mb.openJobs, id)
	mb.ReleasedJobs = append(mb.ReleasedJobs, job)

	select {
	case mb.jobHandled <- void{}:
	default:
	}

	return nil
}

func (mb *mockBackend) IsTimeoutError(err error) bool {
	_, ok := err.(mockTimeout)
	return ok
}

// dvapi.API methods

func (mb *mockBackend) SetEnvironment(env dvapi.Environment) {
	// noop
}

func (mb *mockBackend) GetAdsStatusWithCriteria(criteria map[string]interface{}, requestedInformation []string, parameters map[string]interface{}) (map[uint64]dvapi.AdStatus, error) {
	value, ok := criteria["ids"]
	if !ok {
		return nil, errors.New("Missing adIds criterium")
	}
	adIDs, ok := value.([]uint64)
	if !ok {
		return nil, errors.New("Wrong type for adIds criterium")
	}

	for _, requestable := range requestedInformation {
		switch requestable {
		case "contentId":
		case "siteId":
		case "siteLongCode":
		case "resellerCode":
		case "statusCode":
		case "bvrId":
		case "clientId":
			// Valid
		default:
			return nil, fmt.Errorf("Invalid requestable: %s", requestable)
		}
	}

	ret := make(map[uint64]dvapi.AdStatus)

	for _, adID := range adIDs {
		ret[adID] = dvapi.AdStatus{
			ID:           adID,
			ContentID:    123,
			SiteID:       123,
			SiteLongCode: "testsite",
			ResellerCode: "hexoff",
			StatusCode:   "WILL_BE_ADDED",
		}
	}

	return ret, nil
}

// ReportedAdStatusSource methods

func (mb *mockBackend) Set(adID uint64, statusCode string) (changed bool, err error) {
	current, ok := mb.CurrentAdStatuses[adID]
	bummer.Debugf("Setting adStatus for dvAd(#%d) from %q to %q", adID, current, statusCode)
	if ok && current == statusCode {
		return false, nil
	}
	mb.CurrentAdStatuses[adID] = statusCode
	return true, nil
}

func (mb *mockBackend) Delete(adID uint64) error {
	delete(mb.CurrentAdStatuses, adID)
	return nil
}

// eventclient.Submitter methods

func (mb *mockBackend) Submit(event events.Event) error {
	mb.SubmittedEvents = append(mb.SubmittedEvents, event)
	return nil
}

// Tests

func TestNormalFlow(t *testing.T) {
	if testing.Verbose() {
		bummer.SetBackend(bummer.NewConsoleBackend())
	}

	mb := newMockBackend(t)

	ew := &EventWorker{
		JobSource: mb,
		DVAPI:     mb,
		ReportedAdStatusSource: mb,
		EventsConnection:       mb,
		StatsD:                 stats.Discard(),
	}
	ew.Start()

	mb.HandleJob([]byte(`{"eventType":"dvAdStatusChanged","adIds":[123456789],"stamp":1507037403}`))

	ew.Stop()
	if err := ew.Join(); err != nil {
		t.Error(err)
	}

	if len(mb.SubmittedEvents) != 1 {
		t.Errorf("EventWorker did not submit event for new adStatus")
	}
}
