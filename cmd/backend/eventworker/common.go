package main

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"

	"src.hexon.nl/dv/backend/eventworker"
	"src.hexon.nl/dv/dvapi"
	"src.hexon.nl/events/eventclient"
	"src.hexon.nl/stats"
)

func prepareEventWorker(config *viper.Viper) (*eventworker.EventWorker, error) {
	// Connect to input Beanstalk
	jobSource, err := eventworker.DialBeanstalkJobSource(config.GetString("dv.backend.eventworker.beanstalk.addr"), config.GetString("dv.backend.eventworker.beanstalk.tube"))
	if err != nil {
		return nil, fmt.Errorf("Unable to connect to input Beanstalk: %v", err)
	}

	// Connect to DV frontend
	frontend, err := dvapi.Dial(config.GetString("dv.api.addr"), dvapi.Environment{
		User:        config.GetString("dv.api.user"),
		Application: config.GetString("dv.api.application"),
		Language:    "en",
		Timeout:     5 * time.Minute,
	})
	if err != nil {
		return nil, fmt.Errorf("Unable to connect to dvAPI: %v", err)
	}

	// Connect to database
	db, err := sqlx.Open("mysql", config.GetString("dv.mysql.datasource"))
	if err != nil {
		return nil, fmt.Errorf("Unable to set up connection pool to DV MySQL server: %v", err)
	}

	// Set up last reported ad status source
	lastReportedAdStatusSource, err := eventworker.NewDBReportedAdStatusSource(db)
	if err != nil {
		return nil, fmt.Errorf("Unable to set up last reported ad status source: %v", err)
	}

	// Connect to evsys
	evsys, err := eventclient.Dial(config.GetString("events.beanstalk.addr"), config.GetString("events.beanstalk.tube"))
	if err != nil {
		return nil, fmt.Errorf("Unable to connect to evsys: %v", err)
	}

	// Connect to StatsD
	var statsdInterface stats.Tracker
	if addr := config.GetString("statsd.addr"); addr != "" {
		if statsdClient, err := stats.DialStatsDClient(addr, "dv.eventworker"); err != nil {
			return nil, fmt.Errorf("Unable to connect to StatsD: %v", err)
		} else {
			statsdInterface = statsdClient
			go stats.FlushPeriodically(statsdClient, 2*time.Second, nil)
		}
	} else {
		statsdInterface = stats.Discard()
	}

	// Initialize EventWorker
	ew := &eventworker.EventWorker{
		JobSource: jobSource,
		DVAPI:     frontend,
		ReportedAdStatusSource: lastReportedAdStatusSource,
		EventsConnection:       evsys,
		StatsD:                 statsdInterface,
	}

	return ew, nil
}
