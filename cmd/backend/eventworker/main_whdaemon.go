// +build !standalone

package main

import (
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"

	"github.com/spf13/viper"

	"src.hexon.nl/bummer"
	"src.hexon.nl/whdaemon"
)

func main() {
	// Read in configuration
	config := viper.New()
	config.SetConfigName("eventworker")
	config.AddConfigPath(".")
	config.AddConfigPath("/etc/dv/backend")
	err := config.ReadInConfig()
	if err != nil {
		bummer.Errorf("Error reading config file: %v", err)
		os.Exit(1)
	}

	// Set up Bummer
	bummer.SetDefaultApplication("DV-SxC-EventWorker")
	bummer.AdaptGlobalLogger(bummer.WarningLevel, bummer.With{
		Backtrace: true,
	})
	if bummerServer, err := bummer.DialUDPBackend(config.GetString("bummer.addr")); err != nil {
		bummer.Errorf("Unable to connect to Bummer server: %v", err)
	} else {
		bummer.SetBackend(bummerServer)
	}

	bummer.Debugf("Configuration read from %s", config.ConfigFileUsed())

	// Set up signal handling
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	// HTTP server for pprof
	if httpListen := config.GetString("http-listen"); httpListen != "" {
		go func() {
			err := http.ListenAndServe(httpListen, nil)
			bummer.Errorf("Error running HTTP server on %s (continuing): %v", httpListen, err)
		}()
	}

	// Prepare EventWorker
	ew, err := prepareEventWorker(config)
	if err != nil {
		bummer.Errorf("Error preparing EventWorker: %v", err)
		os.Exit(1)
	}

	// Optionally run as WhDaemon worker
	w := &whdaemon.Worker{
		OnInitialize: func(w *whdaemon.Worker, global bool) {
			w.WaitGroup.Add(1)
			if err := ew.Start(); err != nil {
				bummer.Errorf("Error starting EventWorker in OnInitialize: %v", err)
				os.Exit(1)
			}
			go func() {
				// Wait for EventWorker to finish
				if err := ew.Join(); err != nil {
					bummer.Errorf("EventWorker finished with error: %v", err)
					os.Exit(1)
				}
				w.WaitGroup.Done()
				w.Stop()
			}()
		},
		OnShutdown: func(w *whdaemon.Worker) {
			ew.Stop()
		},
	}
	w.Start()

	// Handle signals
	go func() {
		for s := range signals {
			switch s {
			case os.Interrupt:
				bummer.Debug("Received SIGINT; shutting down...")
				w.Stop()
			}
		}
	}()

	// Wait for process to finish (for whatever reason)
	w.Join()
}
